class User < ApplicationRecord
    validates_presence_of :name, :email, :cpf, :rg, :phone
    validates_uniqueness_of :email, :cpf, :rg 
end
