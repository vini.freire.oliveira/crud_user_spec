require 'rails_helper'

RSpec.describe "Users", type: :request do

  before(:each) do
    10.times do
      create(:user)
    end
  end

  describe "GET /users" do
    it "works!" do
      get users_path
      expect(response).to have_http_status(200)
    end
  end
end
