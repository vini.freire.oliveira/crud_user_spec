require 'rails_helper'

RSpec.describe UsersController, type: :controller do

  before(:each) do
    10.times do
      create(:user)
    end
  end

  describe "GET #index" do
    it "returns a success response" do
      get :index
      expect(response).to be_successful
    end
  end

  describe "GET #show" do
    it "returns a success response" do
      user = User.first
      get :show, params: {id: user.id}
      expect(response).to be_successful
    end
  end

  describe "POST #create" do
    
    context "with valid params" do
      it "creates a new User" do
        expect {
          post :create, params: {user: attributes_for(:user)}
        }.to change(User, :count).by(1)
      end

      it "redirects to the created user" do
        attributes = attributes_for(:user)
        post :create, params: {user: attributes}
        user = User.last
        expect(response).to redirect_to(user)
        expect(user.name).to eql(attributes[:name])
        expect(user.email).to eql(attributes[:email])
        expect(user.cpf).to eql(attributes[:cpf])
        expect(user.email).to eql(attributes[:email])
        expect(user.phone).to eql(attributes[:phone])
      end
    end

  end

  describe "PUT #update" do
    context "with valid params" do

      it "updates the requested user" do
        user = User.last
        new_attributes = attributes_for(:user)
        put :update, params: {id: user.id, user: new_attributes}
        user = User.last
        expect(user.name).to eql(new_attributes[:name])
        expect(user.email).to eql(new_attributes[:email])
        expect(user.cpf).to eql(new_attributes[:cpf])
        expect(user.email).to eql(new_attributes[:email])
        expect(user.phone).to eql(new_attributes[:phone])
      end

      it "redirects to the user" do
        user = User.last
        new_attributes = attributes_for(:user)
        put :update, params: {id: user.id, user: new_attributes}
        expect(response).to redirect_to(user)
      end
    end

  end

  describe "DELETE #destroy" do
    it "destroys the requested user" do
      user = User.last
      expect {
        delete :destroy, params: {id: user.id}
      }.to change(User, :count).by(-1)
    end

    it "redirects to the users list" do
      user = User.last
      delete :destroy, params: {id: user.id}
      expect(response).to redirect_to(users_url)
    end
  end

end
