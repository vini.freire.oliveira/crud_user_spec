require "rails_helper"

RSpec.feature "User submit his data" do
    before do
        @user = attributes_for(:user)
    end

    scenario "user visit the root_path and check all users" do
        
        10.times do
            FactoryBot.create(:user)
        end

        visit  root_path

        User.all.each do |user|
            expect(page).to have_content("Name Email Cpf Rg Phone")
            expect(page).to have_content(user[:name])
            expect(page).to have_content(user[:email])
            expect(page).to have_content(user[:cpf])
            expect(page).to have_content(user[:rg])
            expect(page).to have_content(user[:phone])
        end
        
    end

    scenario "user visit the root_path to show and update each users" do
        
        10.times do
            FactoryBot.create(:user)
        end

        User.all.each do |user|
            visit  root_path + "users/#{user.id}"

            expect(page).to have_content("Name: " + user[:name])
            expect(page).to have_content("Email: " + user[:email])
            expect(page).to have_content("Cpf: " + user[:cpf])
            expect(page).to have_content("Rg: " + user[:rg])
            expect(page).to have_content("Phone: " + user[:phone])

            click_on "Edit"
        
            user_attributes = attributes_for(:user)
            fill_in "Phone", with: user_attributes[:phone]

            click_on "Update User"
            expect(page).to have_content("User was successfully updated")
            expect(page).to have_content("Name: " + user[:name])
            expect(page).to have_content("Email: " + user[:email])
            expect(page).to have_content("Cpf: " + user[:cpf])
            expect(page).to have_content("Rg: " + user[:rg])
            expect(page).to have_content("Phone: " + user_attributes[:phone])


        end
        
    end
    
    scenario "user see the page to create and edit a new user" do
        visit  root_path
        
        click_on "New User"

        fill_in "Name", with: @user[:name]
        fill_in "Email", with: @user[:email]
        fill_in "Cpf", with: @user[:cpf]
        fill_in "Rg", with: @user[:rg]
        fill_in "Phone", with: @user[:phone]
        
        click_on "Create User"

        expect(page).to have_content("User was successfully created")
        expect(page).to have_content("Name: " + @user[:name])
        expect(page).to have_content("Email: " + @user[:email])
        expect(page).to have_content("Cpf: " + @user[:cpf])
        expect(page).to have_content("Rg: " + @user[:rg])
        expect(page).to have_content("Phone: " + @user[:phone])

        click_on "Edit"
        
        user_attributes = attributes_for(:user)
        fill_in "Phone", with: user_attributes[:phone]

        click_on "Update User"
        expect(page).to have_content("User was successfully updated")
        expect(page).to have_content("Name: " + @user[:name])
        expect(page).to have_content("Email: " + @user[:email])
        expect(page).to have_content("Cpf: " + @user[:cpf])
        expect(page).to have_content("Rg: " + @user[:rg])
        expect(page).to have_content("Phone: " + user_attributes[:phone])

    end

end