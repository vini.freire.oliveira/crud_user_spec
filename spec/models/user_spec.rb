require 'rails_helper'

RSpec.describe User, type: :model do
  before(:each) do
    @user = FactoryBot.create(:user)
  end

  describe 'Validations' do
    context 'name' do
        it 'should not be blank' do
            user_repeated = FactoryBot.create(:user)
            user_repeated.email = nil
            expect(user_repeated).to_not be_valid
        end
    end

    context 'cpf' do
      it 'should be unique' do
          user_repeated = FactoryBot.create(:user)
          user_repeated.cpf = @user.cpf
          expect(user_repeated).to_not be_valid
      end

      it 'should not be blank' do
          user_repeated = FactoryBot.create(:user)
          user_repeated.email = nil
          expect(user_repeated).to_not be_valid
      end
    end

    context 'rg' do
      it 'should be unique' do
          user_repeated = FactoryBot.create(:user)
          user_repeated.rg = @user.rg
          expect(user_repeated).to_not be_valid
      end

      it 'should not be blank' do
          user_repeated = FactoryBot.create(:user)
          user_repeated.email = nil
          expect(user_repeated).to_not be_valid
      end
    end

    context 'phone' do
      it 'should not be blank' do
          @user.phone = nil
          expect(@user).to_not be_valid
      end
    end
    
  end 

end
