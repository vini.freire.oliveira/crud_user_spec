FactoryBot.define do
    factory :user do
        email {  FFaker::Internet.unique.email}
        name { FFaker::NameBR.unique.name }
        cpf { FFaker::IdentificationBR.unique.cpf }
        rg { FFaker::IdentificationBR.unique.rg }
        phone { FFaker::PhoneNumberBR.phone_number }
    end
end
